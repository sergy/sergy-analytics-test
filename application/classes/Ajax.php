<?php

class Ajax extends Controller{
	protected $result = array();
			
	function before() {
		parent::before();
		$this->result['success'] = false;
		$this->result['message'] = 'Запрос завершился неудачно';
		$this->response->headers('Content-Type','application/json');
		if(!Request::current()->is_ajax()) $this->response->body(json_encode($this->result));
	}
	
	function after() {
		$this->response->body(json_encode($this->result));
		parent::after();
	}
}

?>
