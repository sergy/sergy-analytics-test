<?php

class Stats extends Controller{
	protected	$model = array(),
				$result = null;
			
	function before() {
		parent::before();
		
		$this->model['user'] = new Model_User();
		$this->model['sites'] = new Model_Sites();
		$this->model['stats'] = new Model_Stats();
	}
	
	public function action_json() {
		$this->response->headers('Content-Type','application/json; charset=utf-8');
		$this->result =  json_encode($this->result,JSON_NUMERIC_CHECK);
	}
	
	public function action_csv() {
		$this->response->headers('Content-Type','text/csv; charset=utf-8');
				
		$res = array();
		foreach($this->result as $line)
		{
			$res[] = implode(',',$line);
		}
		
		$this->result = implode(PHP_EOL,$res);
	}
	
	public function action_xml() {
		$this->response->headers('Content-Type','text/xml; charset=utf-8');
		$xml=new XMLWriter();
		$xml->openMemory();
		$xml->startDocument('1.0','UTF-8');
		$xml->startElement("stat");
		foreach($this->result as $item)
		{
			$xml->startElement("date");
			$xml->writeAttribute("cnt", $item['cnt']);
			$xml->text($item['date']);
			$xml->endElement();
		}
		$xml->endElement();
	
		$this->result = $xml->outputMemory(true);
	}
	
	function after() {
		parent::after();
		$this->response->body($this->result);
	}
	
}

?>
