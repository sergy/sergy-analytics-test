<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax_Dashboard extends Ajax {
	protected $model = array();


	public function before() {
		parent::before();
		$this->model['user'] = new Model_User();
		$this->model['sites'] = new Model_Sites();
	}

	public function action_auth()
	{
		$email = Arr::get($_POST,'email');
		if(filter_var($email,FILTER_VALIDATE_EMAIL))
		{
			if($token = $this->model['user']->getToken($email))
			{
				Session::instance()->set('token',$token);
				Session::instance()->set('email',$email);
				$this->result['success'] = true;
			}
			else {
				$this->result['message'] = 'Не удалось получить токен. Попробуйте позже';
			}
		}
		else
			$this->result['message'] = 'Введите валидный e-mail';
	}
	
	public function action_sites()
	{
		$email = Session::instance()->get('email');
		if(filter_var($email,FILTER_VALIDATE_EMAIL))
		{
			$view = View::factory('ajax/sites')->bind('sites',$sites);
			$sites = $this->model['sites']->getList($email);
			$this->result['html'] = (string) $view;
			$this->result['success'] = true;
		}
		else
			$this->result['message'] = 'Вам нужно заново авторизоваться в системе';
	}
	
	public function action_form()
	{
		$token = Session::instance()->get('token');
		
		if(!empty($token))
		{
			$view = View::factory('ajax/form');
			$this->result['html'] = (string) $view;
			$this->result['success'] = true;
		}
		else
			$this->result['message'] = 'Вам нужно заново авторизоваться в системе';
	}
	
	public function action_save()
	{
		$token = Session::instance()->get('token');
		
		if(!empty($token))
		{
			$domain = Arr::get($_POST,'domain');
			$domain = explode('/',$domain);
			$domain = str_replace('http://','',$domain[0]);
			if(filter_var('http://'.$domain,FILTER_VALIDATE_URL))
			{
				$userId = $this->model['user']->getUserId();
				if($this->model['sites']->checkDomain($userId,$domain))
				{
					if($id = $this->model['sites']->saveDomain($userId,$domain))
					{
						$this->result['id'] = $id;
						$this->result['success'] = true;
					}
					else {
						$this->result['message'] = 'Не удалось добавить сайт. Возможно он уже есть в вашем аккаунте.';
					}
				}
				else {
					$this->result['message'] = 'Не удалось добавить сайт т.к. он уже есть в вашем аккаунте.';
				}
			}
			else
				$this->result['message'] = 'Введите валидный адрес сайта';
		}
		else
			$this->result['message'] = 'Вам нужно заново авторизоваться в системе';
	}
	
	public function action_siteDetail()
	{
		$token = Session::instance()->get('token');
		
		if(!empty($token))
		{
			$id = (int)Request::current()->param('id');
			$userId = $this->model['user']->getUserId();
			if(!empty($id) && $data = $this->model['sites']->getSiteData($id,$userId))
			{
				$event = Arr::get($_POST,'event','visit');
				if(empty($event)) $event = 'visit';
				$data['token'] = $token.'_'.$data['id'];
				$view = View::factory('ajax/siteDetail')->bind('data',$data)->bind('event',$event);
				$this->result['html'] = (string) $view;
				$response = Request::factory('http://analytics.sergy.info/stats/hour/json/'.$event.'/'.$token.'_'.$id)->execute();
				$this->result['stat'] = $response->body();
				$this->result['success'] = true;
			}
			else
				$this->result['message'] = 'Не удалось получить информацию по сайту. Попробуйте позже';
		}
		else
			$this->result['message'] = 'Вам нужно заново авторизоваться в системе';
	}
	
	public function action_siteChart()
	{
		$token = Session::instance()->get('token');
		
		if(!empty($token))
		{
			$id = (int)Request::current()->param('id');
			$userId = $this->model['user']->getUserId();
			if(!empty($id) && $data = $this->model['sites']->getSiteData($id,$userId))
			{
				$event = Arr::get($_POST,'event','visit');
				if(empty($event)) $event = 'visit';
				
				$period = Arr::get($_POST,'period','hour');
				if(empty($period)) $period = 'hour';
				
				$data['token'] = $token.'_'.$data['id'];
				$response = Request::factory('http://analytics.sergy.info/stats/'.$period.'/json/'.$event.'/'.$token.'_'.$id)->execute();
				$this->result['stat'] = $response->body();
				$this->result['success'] = true;
			}
			else
				$this->result['message'] = 'Не удалось получить информацию по сайту. Попробуйте позже';
		}
		else
			$this->result['message'] = 'Вам нужно заново авторизоваться в системе';
	}
	
	public function action_deleteSite()
	{
		$token = Session::instance()->get('token');
		
		if(!empty($token))
		{
			$id = (int)Request::current()->param('id');
			$userId = $this->model['user']->getUserId();
			if(!empty($id) && $data = $this->model['sites']->getSiteData($id,$userId))
			{
				$this->model['sites']->deleteSite($id,$userId);
				$this->result['message'] = 'Сайт <strong>'.$data['domain'].'</strong> успешно удалён';
				$this->result['success'] = true;
			}
			else
				$this->result['message'] = 'Не удалось получить информацию по сайту. Попробуйте позже';
		}
		else
			$this->result['message'] = 'Вам нужно заново авторизоваться в системе';
	}
}