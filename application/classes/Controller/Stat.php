<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Stat extends Controller {
	protected $model = array();
	
	public function before() {
		parent::before();
		$this->model['user'] = new Model_User();
		$this->model['sites'] = new Model_Sites();
	}

	public function action_index() {
		$this->response->headers('Content-Type','application/javascript; charset=utf-8');
		
		$token = htmlspecialchars(strip_tags(Request::current()->param('token')));
		$event = Request::current()->param('event');
		$host = str_replace('/','',str_replace('http://','',Arr::get($_SERVER,'HTTP_REFERER')));
		$useragent = Arr::get($_SERVER,'HTTP_USER_AGENT');
		$ip = Arr::get($_SERVER,'REMOTE_ADDR');
		
		if(!empty($token) && !empty($event) && $id = $this->model['sites']->checkSite($token, $host))
		{
			$this->model['sites']->saveVisit($id,$event,$useragent,$ip);
			$this->response->body('//ok');
		}
		else
		{
			$this->response->body('//fail');
		}
		exit;
	}
}