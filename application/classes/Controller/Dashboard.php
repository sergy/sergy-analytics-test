<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard extends Controller {
	
	protected $auth = false;

	public function before() {
		parent::before();
		$token = Session::instance()->get('token');
		if (!$token && Request::current()->action() != 'auth')
			HTTP::redirect('/dashboard/auth');
		
		if($token)
			$this->auth = true;
	}

	public function action_index() {
		$view = View::factory('template')
				->bind('css',$css)
				->bind('js',$js)
				->bind('pageTitle',$pageTitle)
				->bind('auth',$this->auth)
				->bind('content',$content);
		
		$pageTitle = 'Dashboard';
		
		$dashboard = View::factory('dashboard');
		
		$js = array(
			'/assets/js/raphael/2.1.0/raphael-min.js',
			'/assets/js/morris/morris-0.4.3.min.js',
			//'/assets/js/morris/chart-data-morris.js',
			'/assets/js/application.js',
		);
		
		$css = array(
			'/assets/font-awesome/css/font-awesome.min.css',
			'/assets/css/morris-0.4.3.min.css'
		);
		
		
		$content = (string) $dashboard;
		$page = (string) $view;
		
		$this->response->body($page);
	}

	public function action_auth() {
		$view = View::factory('template')
				->bind('css',$css)
				->bind('js',$js)
				->bind('pageTitle',$pageTitle)
				->bind('auth',$this->auth)
				->bind('content',$content);
		
		$pageTitle = 'Авторизация';
		
		$js = array(
			'/assets/js/auth.js'
		);
		
		
		$content = (string) View::factory('auth');
		$page = (string) $view;
		
		$this->response->body($page);
	}

	public function action_logout() {
		Session::instance()->destroy();
		HTTP::redirect('/dashboard/auth');
	}

}

// End Welcome
