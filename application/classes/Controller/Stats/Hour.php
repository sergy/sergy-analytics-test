<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Stats_Hour extends Stats {

	public function before() {
		parent::before();
		
		$date_start = new DateTime(date('Y-m-d').' 00:00');
		$date_end = clone $date_start;
		$date_end->add(new DateInterval('P1D'));
		
		$token = Request::initial()->param('token');
		$event = base64_encode(Request::initial()->param('event'));
		$token = explode('_',$token);
		$site_id = (int)end($token);
		$user_token = $token[0];
		
		$user_id = $this->model['user']->getUserIdByToken($user_token);
		$this->result = $this->model['stats']->statByHour($date_start,$date_end,$user_id,$site_id,$event);
	}
}