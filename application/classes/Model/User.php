<?php

class Model_User extends Model
{
	const	USERS_TABLE = 'users';
	public $userId = null;

	public function getToken($email)
	{
		if(!empty($email))
		{
			$q = "SELECT `token` FROM `".self::USERS_TABLE."` WHERE `email` = :email LIMIT 1";
			$r = DB::query(Database::SELECT ,$q);
			$r->bind(':email',$email);
			$res = $r->execute()->current();
			if(!empty($res) && !empty($res['token'])) return $res['token'];
			else
			{
				$token = substr(md5($email), 0, 12);
				$q = "INSERT INTO `".self::USERS_TABLE."` SET `email` = :email, `token` = :token, `date_registered` = NOW()";
				$r = DB::query(Database::INSERT ,$q);
				$r->bind(':email',$email);
				$r->bind(':token',$token);
				$r->execute();
				return $token;
			}
		}
		
		return false;
	}
	
	public function getUserId()
	{
		if(is_null($this->userId))
		{
			$email = Session::instance()->get('email');
			$q = "SELECT `id` FROM `".self::USERS_TABLE."` WHERE `email` = :email LIMIT 1";
			$r = DB::query(Database::SELECT ,$q);
			$r->bind(':email',$email);
			$res = $r->execute()->current();
			if(!empty($res) && !empty($res['id']))
				$this->userId = $res['id'];
		}
		
		return $this->userId;
	}
	
	public function getUserIdByToken($token)
	{
		if($token)
		{
			$email = Session::instance()->get('email');
			$q = "SELECT `id` FROM `".self::USERS_TABLE."` WHERE `token` = :token LIMIT 1";
			$r = DB::query(Database::SELECT ,$q);
			$r->bind(':token',$token);
			$res = $r->execute()->current();
			if(!empty($res) && !empty($res['id'])) return $res['id'];
		}
		
		return false;
	}
}

?>
