<?php

class Model_Sites extends Model
{
	const	SITES_TABLE = 'users_sites',
			SITES_STAT_TABLE = 'users_sites_stat',
			USERS_TABLE = 'users';
	
	public function getList($email)
	{
		if(!empty($email))
		{
			$q = "SELECT s.* FROM `".self::SITES_TABLE."` as s JOIN `".self::USERS_TABLE."` as u ON s.user_id = u.id WHERE u.`email` = :email";
			$r = DB::query(Database::SELECT ,$q);
			$r->bind(':email',$email);
			$res = $r->execute()->as_array();
			if(!empty($res)) return $res;
		}
		
		return false;
	}
	
	public function checkDomain($id,$domain)
	{
		$q = "SELECT COUNT(1) as `c` FROM `".self::SITES_TABLE."`WHERE `domain` = :domain AND `user_id` = :id";
		$r = DB::query(Database::SELECT ,$q);
		$r->bind(':id', $id);
		$r->bind(':domain', $domain);
		$res = $r->execute()->current();
		if(!empty($res) && $res['c'] > 0) return false;
		else return true;
	}
	
	public function getSiteData($id,$userId)
	{
		if(!empty($userId) && !empty($id))
		{
			$q = "SELECT * FROM `".self::SITES_TABLE."`WHERE `id` = :id AND `user_id` = :userId";
			$r = DB::query(Database::SELECT ,$q);
			$r->bind(':id', $id);
			$r->bind(':userId', $userId);
			$res = $r->execute()->current();
		}
		
		if(!empty($res)) return $res;
		else return true;
	}
	
	public function deleteSite($id,$userId)
	{
		if(!empty($userId) && !empty($id))
		{
			$q = "DELETE FROM `".self::SITES_TABLE."`WHERE `id` = :id AND `user_id` = :userId";
			$r = DB::query(Database::DELETE ,$q);
			$r->bind(':id', $id);
			$r->bind(':userId', $userId);
			$res = $r->execute();
			
			$q = "DELETE FROM `".self::SITES_STAT_TABLE."`WHERE `site_id` = :id";
			$r = DB::query(Database::DELETE ,$q);
			$r->bind(':id', $id);
			$res = $r->execute();
		}
		
		return true;
	}
	
	public function saveDomain($id,$domain)
	{
		if(!empty($domain) && !empty($id))
		{
			$q = "INSERT INTO `".self::SITES_TABLE."` SET `user_id` = :id, `domain` = :domain";
			$r = DB::query(Database::INSERT ,$q);
			$r->bind(':id', $id);
			$r->bind(':domain', $domain);
			list($insert_id, $total_rows_affected) = $r->execute();
			return $insert_id;
		}
		
		return false;
	}
	
	public function checkSite($token,$domain)
	{
		$token = explode('_',$token);
		$id = end($token);
		$q = "SELECT * FROM `".self::SITES_TABLE."`WHERE `id` = :id";
		$r = DB::query(Database::SELECT ,$q);
		$r->bind(':id', $id);
		
		$res = $r->execute()->current();
		
		if(!empty($res)) return $id;
		else return false;
	}
	
	public function saveVisit($id,$event,$useragent,$ip)
	{
		if(!empty($id) && !empty($event))
		{
			$time = time();
			$q = "INSERT INTO `".self::SITES_STAT_TABLE."` SET `site_id` = :id, `event` = :event, `timestamp` = :time, `useragent` = :useragent, `ip` = :ip";
			$r = DB::query(Database::INSERT ,$q);
			$r->bind(':id', $id);
			$r->bind(':event', $event);
			$r->bind(':time', $time);
			$r->bind(':useragent', $useragent);
			$r->bind(':ip', $ip);
			$r->execute();
		}
	}
	
}

?>
