<?php

class Model_Stats extends Model
{
	const	SITES_TABLE = 'users_sites',
			SITES_STAT_TABLE = 'users_sites_stat';
	
	public function statByHour($start,$end,$user_id,$site_id,$event)
	{
		$start = $start->getTimestamp();
		$end = $end->getTimestamp();
		if(!empty($user_id) && !empty($site_id))
		{
			$q = "SELECT HOUR(FROM_UNIXTIME(s.`timestamp`)) as `date`,COUNT(1) as `cnt` FROM `".self::SITES_STAT_TABLE."` s JOIN `".self::SITES_TABLE."` u ON s.`site_id` = u.id WHERE u.`user_id` = :user AND s.`site_id` = :site AND s.timestamp >= :start AND s.timestamp < :end AND s.event = :event GROUP BY `date`";
			$r = DB::query(Database::SELECT ,$q);
			$r->bind(':user',$user_id);
			$r->bind(':site',$site_id);
			$r->bind(':start',$start);
			$r->bind(':end',$end);
			$r->bind(':event',$event);
			
			$res = $r->execute()->as_array();
			if(!empty($res)) {
				return $res;
			}
		}
		
		return false;
	}
	
	public function statByDay($start,$end,$user_id,$site_id,$event)
	{
		$start = $start->getTimestamp();
		$end = $end->getTimestamp();
		if(!empty($user_id) && !empty($site_id))
		{
			$q = "SELECT DAYNAME(FROM_UNIXTIME(s.`timestamp`)) as `date`,COUNT(1) as `cnt` FROM `".self::SITES_STAT_TABLE."` s JOIN `".self::SITES_TABLE."` u ON s.`site_id` = u.id WHERE u.`user_id` = :user AND s.`site_id` = :site AND s.timestamp >= :start AND s.timestamp < :end AND s.event = :event GROUP BY `date`";
			$r = DB::query(Database::SELECT ,$q);
			$r->bind(':user',$user_id);
			$r->bind(':site',$site_id);
			$r->bind(':start',$start);
			$r->bind(':end',$end);
			$r->bind(':event',$event);
			
			$res = $r->execute()->as_array();
			if(!empty($res)) return $res;
		}
		
		return false;
	}
	
	public function statByWeek($start,$end,$user_id,$site_id,$event)
	{
		$start = $start->getTimestamp();
		$end = $end->getTimestamp();
		if(!empty($user_id) && !empty($site_id))
		{
			$q = "SELECT WEEK(FROM_UNIXTIME(s.`timestamp`),1) as `date`,COUNT(1) as `cnt` FROM `".self::SITES_STAT_TABLE."` s JOIN `".self::SITES_TABLE."` u ON s.`site_id` = u.id WHERE u.`user_id` = :user AND s.`site_id` = :site AND s.timestamp >= :start AND s.timestamp < :end AND s.event = :event GROUP BY `date`";
			$r = DB::query(Database::SELECT ,$q);
			$r->bind(':user',$user_id);
			$r->bind(':site',$site_id);
			$r->bind(':start',$start);
			$r->bind(':end',$end);
			$r->bind(':event',$event);
			
			$res = $r->execute()->as_array();
			if(!empty($res)) return $res;
		}
		
		return false;
	}
}

?>
