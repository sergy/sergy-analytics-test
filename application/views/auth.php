<div class="col-lg-2"></div>
<div class="col-lg-6">
	<h1>Авторизация</h1>
	<div class="progress progress-striped active hidden" id="progressBar">
		<div class="progress-bar" style="width: 90%"></div>
	</div>
	<p>Введите свой e-mail. Он будет использован для формирования универсального токена</p>
	<form role="form" class="dl-horizontal" method="post" id="authForm">
		<div class="form-group input-group">
			<span class="input-group-addon">@</span>
			<input type="text" class="form-control" name="email" placeholder="E-mail" id="authEmail">
		</div>
		<button type="submit" class="btn btn-default">Войти</button>
	</form>
</div>