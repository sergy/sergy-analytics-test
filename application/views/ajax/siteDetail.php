<div class="row">
	<div class="col-lg-4">
		<div class="panel panel-primary">
			<div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-long-arrow-right"></i> Код отслеживания для этого ресурса</h3>
			</div>
			<div class="panel-body">
				<div class="form-group input-group">
					<span class="input-group-addon">Событие</span>
					<input type="text" class="form-control" value="<?= $event ?>" id="siteEvent" name="event">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button" id="siteEventButton" data-id="<?= $data['id'] ?>" >применить</button>
					</span>
				</div>
				<div class="form-group input-group">
					<span class="input-group-addon">Token</span>
					<input type="text" class="form-control" value="<?= $data['token'] ?>" disabled="disabled">
				</div>
				<strong>Код для отслеживания:</strong>
				<div>
					<code style="white-space: normal;"><?php
						$event = base64_encode($event);
						$code = <<<eof
<script type="text/javascript">
(function() {
  var sa = document.createElement('script');
  sa.type = 'text/javascript'; sa.async = true;
  sa.src = 'http://analytics.sergy.info/stat/';
  sa.src += '{$data['token']}/';
  sa.src += '{$event}/';
  var s = document.getElementsByTagName('script')[0];
  s.parentNode.insertBefore(sa, s);
})();
</script>
eof;
						echo htmlspecialchars($code)
						?></code>
				</div>
                <div class="text-right">
					<a href="#" data-id="<?= $data['id'] ?>" id="deleteDomain">Удалить ресурс <i class="fa fa-trash-o text-danger"></i></a>
                </div>
			</div>
		</div>
	</div>
	<div class="col-lg-8">
		<div class="panel panel-primary">
			<div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-long-arrow-right"></i> Статистика посещаемости ресурса</h3>
			</div>
			<div class="panel-body">
				<div id="morris-chart-line"></div>

                <div class="text-right" id="statPeriod">
					<a href="#" data-id="<?= $data['id'] ?>" data-type="hour" class="label label-primary">По часам</a> |
					<a href="#" data-id="<?= $data['id'] ?>" data-type="day" class="label label-default">По дням</a> |
					<a href="#" data-id="<?= $data['id'] ?>" data-type="week" class="label label-default">По неделям</a>
                </div>
			</div>
		</div>
	</div>
</div>