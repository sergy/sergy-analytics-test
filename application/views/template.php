<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">

		<title>Analytics::<?= $pageTitle ?></title>

		<link href="/assets/css/bootstrap.css" rel="stylesheet">
		<link href="/assets/css/sb-admin.css" rel="stylesheet">
		<link href="/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
		<link href="/assets/css/morris-0.4.3.min.css" rel="stylesheet">
		<?php if($css) foreach($css as $file) echo HTML::style($file)."\r\n"; ?>
	</head>
	<body>
		<div id="wrapper">
			<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
				<div class="navbar-header">
					<?php if($auth): ?>
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
						<span class="sr-only">Показать меню</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					  </button>
					<?php endif; ?>
					<a class="navbar-brand" href="/">Analytics by Sergei Porotikov</a>
				</div>
			<?php if($auth): ?>
				<div class="collapse navbar-collapse navbar-ex1-collapse">
					<ul class="nav navbar-nav side-nav" id="sites">
						<li class="active"><a href="/dashboard/new"><i class="fa fa-file"></i> Новый сайт</a></li>
					</ul>

					<ul class="nav navbar-nav navbar-right navbar-user">
						<li class="dropdown messages-dropdown">
							<a href="/dashboard/logout"><i class="fa fa-power-off"></i> Выйти [<?= Session::instance()->get('email') ?>]</a>
						</li>
					</ul>
				</div><!-- /.navbar-collapse -->
			<?php endif; ?>
			</nav>
			<div id="page-wrapper">
				<?= $content ?>
			</div>
		</div>
		
		<script src="/assets/js/jquery-1.10.2.js"></script>
		<script src="/assets/js/bootstrap.min.js"></script>
		
		<?php if($js) foreach($js as $file) echo HTML::script($file)."\r\n"; ?>
	</body>
</html>
