<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-dashboard"></i> Dashboard</li>
			<li class="active">Добавление ресурса</li>
		</ol>
		<div class="progress progress-striped active hidden" id="progressBar">
			<div class="progress-bar" style="width: 90%"></div>
		</div>
		<div id="mainContainer"></div>
	</div>
</div><!-- /.row -->