<?php defined('SYSPATH') OR die('No direct access allowed.');
	$dsn = '';
	$user = '';
	$password = '';
	
if (Kohana::$environment === Kohana::DEVELOPMENT) {
    $dsn = 'mysql:host=localhost;dbname=sergy_analytics';
	$user = 'root';
	$password = '';
} elseif (Kohana::$environment === Kohana::TESTING) {
    $dsn = 'mysql:host=sergy.mysql.ukraine.com.ua;dbname=sergy_analytics';
	$user = 'sergy_analytics';
	$password = 'sclqu53n';
}
 elseif (Kohana::$environment === Kohana::PRODUCTION) {
    $dsn = 'mysql:host=sergy.mysql.ukraine.com.ua;dbname=sergy_analytics';
	$user = 'sergy_analytics';
	$password = 'sclqu53n';
}

return array
	(
		'default' => array
		(
		'type'       => 'PDO',
			'connection' => array(
				'dsn'        => $dsn,
				'username'   => $user,
				'password'   => $password,
				'persistent' => FALSE,
			),
			'table_prefix' => '',
			'charset'      => 'utf8',
			'caching'      => FALSE,
		)
	);
