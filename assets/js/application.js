var app = {
	pregressBar: null,
	sitesContainer: null,
	mainContainer: null,
	chart: null,
	init: function()
	{
		var me = this;

		me.progressBar = $('#progressBar');
		me.sitesContainer = $('#sites');
		me.mainContainer = $('#mainContainer');
		me.loadSites();
		me.loadNewForm();

		me.sitesContainer.on('click', 'a', function(e) {
			e.preventDefault();
			var id = $(this).data('id');
			if (id)
				me.loadSiteDetail(id);
			else
				me.loadNewForm();
		});

		me.mainContainer.on('click', '#deleteDomain', function(e) {
			e.preventDefault();
			var id = $(this).data('id');
			if (id && confirm('Подтвердите удаление выбранного сайта'))
				me.deleteSite(id);
		});

		me.mainContainer.on('click', '#siteEventButton', function(e) {
			e.preventDefault();
			var id = $(this).data('id');
			if (id)
				me.loadSiteDetail(id);
		});
		
		me.mainContainer.on('click', '#statPeriod a', function(e) {
			e.preventDefault();
			var id = $(this).data('id');
			var period = $(this).data('type');
			$('#statPeriod').find('a').removeClass('label-primary').addClass('label-default');
			$(this).removeClass('label-default').addClass('label-primary');
			if (id)
				me.updateChart(id,period);
		});
		
		
	},
	deleteSite: function(id) {
		var me = this;

		$.ajax({
			url: "/ajax/dashboard/deleteSite/" + id,
			dataType: 'json',
			type: 'GET',
			beforeSend: function(xhr) {
				xhr.overrideMimeType("application/json");

				me.progressBar.removeClass('hidden');
			}
		}).done(function(json) {
			me.progressBar.addClass('hidden');

			if (json && json.success && json.success === true)
			{
				me.mainContainer.html(json.message);
				$('.breadcrumb .active').text('Сайт и его статистика удалены');
				me.loadSites();
			}
			else if (json && json.message)
			{
				alert(json.message);
			}
			else
			{
				alert('Не удалось удалить ресурс. Попробуйте позже');
			}

		}).error(function() {
			me.progressBar.addClass('hidden');
			alert('Не удалось удалить ресурс. Попробуйте позже');
		});

	},
	loadSiteDetail: function(id) {
		var me = this;
		var event = $('#siteEvent').val() || '';
		$.ajax({
			url: "/ajax/dashboard/siteDetail/" + id,
			dataType: 'json',
			type: 'POST',
			data: 'event=' + event,
			beforeSend: function(xhr) {
				xhr.overrideMimeType("application/json");

				me.progressBar.removeClass('hidden').find('.progress-bar').css('width','50%');
			}
		}).done(function(json) {
			me.progressBar.find('.progress-bar').css('width','90%');

			if (json && json.success && json.success === true)
			{
				me.mainContainer.html(json.html);
				me.sitesContainer.find('li').removeClass('active');
				me.sitesContainer.find('a[data-id=' + id + ']').parents('li:eq(0)').addClass('active');
				
				me.chart = Morris.Bar({
					element: 'morris-chart-line',
					data: [{date:0,cnt: 0}],
					xkey: 'date',
					ykeys: ['cnt'],
					labels: ['Посещений'],
					barRatio: 0.4,
					hideHover: 'auto'
				});
				
				if (json.stat && json.stat != 'false')
					me.chart.setData($.parseJSON(json.stat));
				
				$('.breadcrumb .active').text('Статистика и информация по ресурсу');

			}
			else if (json && json.message)
			{
				alert(json.message);
			}
			else
			{
				alert('Не удалось загрузить информацию по ресурсу. Попробуйте позже');
			}
			me.progressBar.addClass('hidden');
		}).error(function() {
			me.progressBar.find('.progress-bar').css('width','90%');
			me.progressBar.addClass('hidden');
			alert('Не удалось загрузить информацию по ресурсу. Попробуйте позже');
		});

	},
	updateChart: function(id,period) {
		var me = this;
		var event = $('#siteEvent').val() || '';
		$.ajax({
			url: "/ajax/dashboard/siteChart/" + id,
			dataType: 'json',
			type: 'POST',
			data: 'event=' + event + '&period='+period,
			beforeSend: function(xhr) {
				xhr.overrideMimeType("application/json");

				me.progressBar.removeClass('hidden').find('.progress-bar').css('width','50%');
			}
		}).done(function(json) {
			me.progressBar.find('.progress-bar').css('width','90%');

			if (json && json.success && json.success === true)
			{
				if (json.stat && json.stat != 'false')
					me.chart.setData($.parseJSON(json.stat));
			}
			else if (json && json.message)
			{
				alert(json.message);
			}
			else
			{
				alert('Не удалось загрузить информацию по ресурсу. Попробуйте позже');
			}
			me.progressBar.addClass('hidden');
		}).error(function() {
			me.progressBar.find('.progress-bar').css('width','90%');
			me.progressBar.addClass('hidden');
			alert('Не удалось загрузить информацию по ресурсу. Попробуйте позже');
		});

	},
	loadNewForm: function()
	{
		var me = this;
		$.ajax({
			url: "/ajax/dashboard/form",
			dataType: 'json',
			type: 'GET',
			beforeSend: function(xhr) {
				xhr.overrideMimeType("application/json");

				me.progressBar.removeClass('hidden');
			}
		}).done(function(json) {
			me.progressBar.addClass('hidden');

			if (json && json.success && json.success === true)
			{
				me.mainContainer.html(json.html);
				$('.breadcrumb .active').text('Добавление ресурса');
				me.sitesContainer.find('li').removeClass('active');
				me.sitesContainer.find('li:eq(0)').addClass('active');
				$('#newForm').submit(function(e) {
					e.preventDefault();
					var siteField = $('#newFormDomain');
					var button = $('#newForm').find('button[type=submit]:eq(0)');
					$.ajax({
						url: "/ajax/dashboard/save",
						dataType: 'json',
						data: $('#newForm').serialize(),
						type: 'POST',
						beforeSend: function(xhr) {
							xhr.overrideMimeType("application/json");

							me.progressBar.removeClass('hidden');
							siteField.attr('disabled', true);
							button.attr('disabled', true);
						}
					}).done(function(json) {
						me.progressBar.addClass('hidden');
						siteField.attr('disabled', false);
						button.attr('disabled', false);

						if (json && json.success && json.success === true)
						{
							me.loadSites(json.id);

						}
						else if (json && json.message)
						{
							alert(json.message);
						}
						else
						{
							alert('Не удалось сохарнить новый сайт. Попробуйте позже');
						}

					}).error(function() {
						siteField.attr('disabled', false);
						button.attr('disabled', false);
						me.progressBar.addClass('hidden');
						alert('Не удалось сохарнить новый сайт. Попробуйте позже');
					});
				});
			}
			else if (json && json.message)
			{
				alert(json.message);
			}
			else
			{
				alert('Не удалось загрузить форму добавления сайта. Попробуйте позже');
			}

		}).error(function() {
			me.progressBar.addClass('hidden');
			alert('Не удалось загрузить форму добавления сайта. Попробуйте позже');
		});

	},
	loadSites: function(loadId)
	{
		var me = this;

		$.ajax({
			url: "/ajax/dashboard/sites",
			dataType: 'json',
			type: 'GET',
			beforeSend: function(xhr) {
				xhr.overrideMimeType("application/json");

				me.progressBar.removeClass('hidden');
			}
		}).done(function(json) {
			me.progressBar.addClass('hidden');

			if (json && json.success && json.success === true)
			{
				me.sitesContainer.html(json.html);
				if (loadId)
					me.loadSiteDetail(loadId);
			}
			else if (json && json.message)
			{
				alert(json.message);
			}
			else
			{
				alert('Не удалось получить список ваших сайтов. Попробуйте позже');
			}

		}).error(function() {
			me.progressBar.addClass('hidden');
			alert('Не удалось получить список ваших сайтов. Попробуйте позже');
		});
	}
}

$(document).ready(function() {
	app.init();
});