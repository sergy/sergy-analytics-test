$(document).ready(function(){
	var authForm = $('#authForm');
	
	authForm.submit(function(e){
		e.preventDefault();
		var emailField = $('#authEmail');
		var progressBar = $('#progressBar');
		var button = authForm.find('button[type=submit]:eq(0)');
		var val = emailField.val();
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		if(val != '' && re.test(val))
		{
			emailField.parents('.form-group:eq(0)').removeClass('has-error');
			$.ajax({
				url: "/ajax/dashboard/auth",
				dataType: 'json',
				data: authForm.serialize(),
				type: 'POST',
				beforeSend: function(xhr) {
					xhr.overrideMimeType("application/json");
					
					progressBar.removeClass('hidden');
					emailField.attr('disabled',true);
					button.attr('disabled',true);
				}
			}).done(function(json) {
				progressBar.addClass('hidden');
				emailField.attr('disabled',false);
				button.attr('disabled',false);
				
				if (json && json.success && json.success === true)
				{
					document.location.href = '/';
				}
				else if(json && json.message)
				{
					alert(json.message);
				}
				else
				{
					alert('Не удалось авторизоваться. Попробуйте позже');
				}

			}).error(function() {
				progressBar.addClass('hidden');
				emailField.attr('disabled',false);
				button.attr('disabled',false);
				alert('Не удалось авторизоваться. Попробуйте позже');
			});
		}
		else
		{
			emailField.parents('.form-group:eq(0)').addClass('has-error');
		}
	});
});